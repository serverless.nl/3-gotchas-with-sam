## Test the python index file

`pytest -vv`

## Test the clopudformation template

`cfn-lint --cfn_file template.yml`

    cfn-lint: AWS CloudFormation Linter`
    Linting template.yml
    Unsupported resource: CreateSomething (AWS::Serverless::Function)
    Unsupported resource: DeleteSomething (AWS::Serverless::Function)
    Unsupported resource: SomethingApi (AWS::Serverless::Api)

## Validate the tamplate

`aws cloudformation validate-template --template-body file://template.yml`

    {
        "Parameters": [],
        "Description": "API to create and delete something."
    }

## Package

`aws cloudformation package --template-file template.yml --s3-bucket tdv-artifacts --output-template-file parsed_template.yml`

    Uploading to f20315939e817ccbdce0feab6ad50804  456 / 456.0  (100.00%)
    Successfully packaged artifacts and wrote output template to file parsed_template.yml.


## Deploy the packaged file

`aws cloudformation deploy --template-file parsed_template.yml --stack-name something-api --capabilities CAPABILITY_IAM`

    Waiting for changeset to be created..
    Waiting for stack create/update to complete
    Successfully created/updated stack - something-api

## See what's been created

`aws cloudformation describe-stacks --stack-name something-api`