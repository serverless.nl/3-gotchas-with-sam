import logging
import json

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))

def create_something(event, context):
    logging.info("Event: {}".format(event))

    return {"body": json.dumps({"hello": "world"}),
            "statusCode": 200}


def delete_something(event, context):
    logging.info("Event: {}".format(event))

    try:
        pass
    except Exception as e:
        return {"statusCode": 500, "body": json.dumps({"error": str(e)})}
    return {"statusCode": 200, "body": json.dumps({"result": "DELETED", "hello": "world"})}
