import logging
import index

######################################################
# Tests                                              #
######################################################

def test_create_something():
    logging.info("Create test")
    assert index.create_something({}, {}) == {'body': '{"hello": "world"}', 'statusCode': 200}

def test_delete_something():
    logging.info("Delete test")
    index.delete_something({}, {})
    assert index.delete_something({}, {}) == {'body': '{"result": "DELETED", "hello": "world"}', 'statusCode': 200}
